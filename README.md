# pokedex

A simple CRUD for itvws tests. Probably will be updated in the future.  
For now, infinite scroll does not work properly. Some bugs are there of course.  
My first time dabbling with GQL as well so it took a lot of time.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
