import gql from 'graphql-tag'

const pokemonTypeQuery = `pokemon_v2_pokemons: {
  pokemon_v2_pokemontypes: {
    pokemon_v2_type: {
      name: {
        _in: $pokemonTypes
      }
    }
  }
}
`
const pokemonGenQuery = `pokemon_v2_generation: {
  name: {
    _in: $pokemonGens
  }
}
`
const legendaryQuery = `is_legendary: {
  _eq: true
}`
const mythicalQuery = `is_mythical: {
  _eq: true
}`

const nameQuery = `name: {
  _in: $compareList
}`

const getPokemons = (pokemonTypes, pokemonGens, isLegendary, isMythical, isComparing) => {
  const shouldIncludeLegendaryMythicalQuery = isLegendary || isMythical
  const whereQuery = `
    ${pokemonTypes.length > 0 ? pokemonTypeQuery : ''}
    ${pokemonGens.length > 0 ? pokemonGenQuery : ''}
    ${shouldIncludeLegendaryMythicalQuery && isLegendary ? legendaryQuery : ''}
    ${shouldIncludeLegendaryMythicalQuery && isMythical ? mythicalQuery : ''}
    ${isComparing ? nameQuery : ''}
  `

  return gql`
    query getPokemons(
      $limit: Int! = 20,
      $offset: Int!,
      $pokemonTypes: [String!],
      $pokemonGens: [String!],
      $isComparing: Boolean!,
      $compareList: [String!]
      ) {
        species: pokemon_v2_pokemonspecies(
          limit: $limit
          offset: $offset
          order_by: {id: asc}
          where: {
            ${whereQuery}
          }
        ) {
            id
            name
            is_legendary
            is_mythical
            pokemons: pokemon_v2_pokemons {
              id
              types: pokemon_v2_pokemontypes {
                type: pokemon_v2_type {
                  name
                }
              }
              stats: pokemon_v2_pokemonstats @include(if: $isComparing) {
                base_stat
                stat: pokemon_v2_stat {
                  name
                }
              }
            }
          }
          
        species_aggregate: pokemon_v2_pokemonspecies_aggregate(
          where: {
            ${whereQuery}
          }
        ) {
          aggregate {
            count
          }
        }
      }
  `
}

export default getPokemons
