export const POKEMON_TYPES = [
  { name: 'normal', selected: false },
  { name: 'fighting', selected: false },
  { name: 'flying', selected: false },
  { name: 'poison', selected: false },
  { name: 'ground', selected: false },
  { name: 'rock', selected: false },

  { name: 'bug', selected: false },
  { name: 'ghost', selected: false },
  { name: 'steel', selected: false },
  { name: 'fire', selected: false },
  { name: 'water', selected: false },
  { name: 'grass', selected: false },

  { name: 'electric', selected: false },
  { name: 'psychic', selected: false },
  { name: 'ice', selected: false },
  { name: 'dragon', selected: false },
  { name: 'dark', selected: false },
  { name: 'fairy', selected: false }
]

export const POKEMON_GENS = [
  { name: 'generation-i', selected: false },
  { name: 'generation-ii', selected: false },
  { name: 'generation-iii', selected: false },
  { name: 'generation-iv', selected: false },
  { name: 'generation-v', selected: false },
  { name: 'generation-vi', selected: false },
  { name: 'generation-vii', selected: false }
]
